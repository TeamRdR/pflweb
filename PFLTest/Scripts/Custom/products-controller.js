﻿angular.module("PFLApp", [])
    .controller("ContrProducts", function($scope, $http)
    {
        $scope.model;
        $scope.Order = null;
        $scope.Status = "";
        $scope.Shipping = [];
        $scope.ListProducts = [];

        $scope.LoadProducts = function()
        {
            $scope.Status = "Loading Products...";
            $scope.ListProducts = [];

            let caller = $http.get("/product/list");

            caller.then(function(response)
            {
                $scope.Status = null;
                $scope.ListProducts = response.data;
            },
            function(data)
            {
                $scope.Status = "Honk!  Something went wrong!";
            });
        };

        $scope.SearchProds = function()
        {
            let who;
            let dudes = $("#dvProdList").children();
            let title;
            let findMe = $("#inProdSearch").val().toLowerCase();
            let cleared = IsEmpty(findMe);

            for (let xlp = 0; xlp < dudes.length; xlp++)
            {
                who = $(dudes[xlp]);

                if (cleared)
                {
                    who.show();
                    continue;
                }

                title = who.find("h3").html();
                title = title.toLowerCase();

                if (title.indexOf(findMe) === -1)
                {
                    who.hide();
                }
                else
                {
                    who.show();
                }
            }

            $("#inProdSearch").val("");
        };

        $scope.OrderIt = function(product)
        {
            $scope.Status = "Loading Product...";
            $scope.Product = {};
            $scope.Shipping = [];

            let caller = $http.get("/product/detail/" + product.productID);

            caller.then(function(response)
            {
                let data = response.data;

                $scope.Status = null;
                $scope.Product = data;

                BuildShipping($scope, data.deliveredPrices);

                if (IsEmpty(data.quantityDefault))
                {
                    $("#inOrderQty").val("0");
                }
                else
                {
                    $("#inOrderQty").val(data.quantityDefault);
                }

                if ( response.data.hasTemplate )
                {
                    BuildExtraFields(data.templateFields.fieldlist.field);
                }
            },
            function(repsonse)
            {
                $scope.Status = "Honk!  Something went wrong!";
            });

            $("#dvProdList").hide();
            $("#dvProdDetail").show();
        };

        $scope.ShowList = function()
        {
            $("#dvProdList").show();
            $("#dvProdDetail").hide();
            $("#dvOrderExtra").empty();
            $("#dvOrderComplete").hide();
        };

        $scope.ValidateOrder = function()
        {
console.log($scope.model);

            let caller = $http({ method: "POST", url: "/product/order", data: $scope.model });

            caller.then(function(response)
            {
                $scope.Status = null;
            },
            function(data)
            {
                $scope.Status = "Honk!  Something went wrong!";
            });
        };
    });

function BuildShipping($scope, data)
{
    let dad = $("#cbOrderShip");
    let codeExists = function(what)
    {
        for (let xlp = 0; xlp < $scope.Shipping.length; xlp++)
        {
            if ($scope.Shipping[xlp].deliveryMethodCode === what)
            {
                return true;
            }
        }

        return false;
    };

    dad.empty();

    if (IsEmpty(data))
    {
        return;
    }

    for (let xlp = 0; xlp < data.length; xlp++)
    {
        if (codeExists(data[xlp].deliveryMethodCode))
        {
            continue;
        }

        let opt = new Option(data[xlp].description, data[xlp].deliveryMethodCode, data[xlp].isDefault);

        dad.append(opt);

        $scope.Shipping.push(data[xlp]);
    }
}

function BuildExtraFields(data)
{
    var OPT_NO = "N";
    var OPT_YES = "Y";

    var TYPE_FILE = "GRAPHICPICKLIST";
    var TYPE_TEXTBOX = "SINGLELINE";
    var TYPE_TEXTAREA = "MULTILINE";
    var TYPE_SEPARATOR = "SEPARATOR";

    var dvExtra = $("#dvOrderExtra");

    dvExtra.empty();

    if (IsEmpty(data))
    {
        dvExtra.hide();
        return;
    }

    var dad;
    var lab;
    var who;
    var field;
    var dvField;

    for (let xlp = 0; xlp < data.length; xlp++)
    {
        who = data[xlp];

        if (who.type === TYPE_SEPARATOR)
        {
            field = $("<hr />");
            field.addClass("col-sm-12");

            dad = $("<div>");
            dad.append(field);

            dvExtra.append(dad);
            continue;
        }

        lab = $("<label>");
        lab.html(who.fieldname);
        lab.addClass("col-sm-3 col-form-label");

        switch (who.type)
        {
            case TYPE_FILE:
                field = $("<input>");
                field.attr("type", "file");
                break;

            case TYPE_TEXTBOX:
                field = $("<input>");
                field.val(who.default);
                field.attr("type", "text");
                break;

            case TYPE_TEXTAREA:
                field = $("<textarea>");
                field.val(who.default);
                field.attr("rows", who.linelimit);
                break;
        }

        field.attr("name", who.htmlfieldname);
        field.attr("maxlength", who.charlimit);
        field.attr("placeholder", who.prompt[0].text);      // TODO:  Don't hard-code language field
        field.addClass("form-control");

        if (who.required === OPT_YES)
        {
            lab.addClass("required");
            field.prop("required", true);
        }

        dvField = $("<div>");
        dvField.append(field);
        dvField.addClass("col-sm-9");

        dad = $("<div>");
        dad.append(lab);
        dad.append(dvField);
        dad.addClass("form-group");

        if (who.visible === OPT_NO)
        {
            dad.css("display", "none");
        }

        dvExtra.append(dad);
    }

    dvExtra.show();
}