﻿function IsEmpty(iDude)
{
    if (typeof iDude === "undefined" || iDude === undefined || iDude === null || iDude === "")
    {
        return true;
    }

    if (typeof iDude === "boolean")
    {
        return (iDude === false);
    }

    if (iDude instanceof Array)
    {
        return (iDude.length === 0);
    }

    var xStr = iDude.toString().toLowerCase();

    if (xStr === "0" || xStr === "false" || xStr === "&nbsp;" || xStr === "0000-00-00")
    {
        return true;
    }

    return false;
}