﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using Newtonsoft.Json;
using PFL.Models;

namespace PFL.Utils
{
    public enum StatusType
    {
        Info,
        Error,
        Success
    }

    public class PFLComm
    {

#region -----  Event stuff  -----
        public delegate void StatusChangeDelegate(StatusChangedArgs args);
        public event StatusChangeDelegate StatusChanged;
#endregion

        // TODO:  Config file this
        private string DataURL = "https://testapi.pfl.com/";
        private string authUser = "miniproject";
        private string authPass = "Pr!nt123";

        public int Timeout = 20000;
        public string FullURL;
        public string CallFunction;
        public ResultSet CallResult;
        public Dictionary<string, object> PostParms;

        private List<object> callParms;
        private Dictionary<string, object> urlVars;

        public object Data
        {
            get
            {
                return this.CallResult.Data;
            }
        }

        public int Status
        {
            get
            {
                return this.CallResult.Status;
            }
        }

        public string Message
        {
            get
            {
                return this.CallResult.Message;
            }
        }

        public PFLComm()
        {
            this.Reset();
        }

        public PFLComm(string callFunction)
        {
            this.Reset();

            this.CallFunction = callFunction;
        }

        public void AddURLParameter(object value)
        {
            this.callParms.Add(value);
        }

        public void AddURLVar(string key, object value)
        {
            if ( this.urlVars.ContainsKey(key) )
            {
                this.urlVars[key] = value;
            }
            else
            {
                this.urlVars.Add(key, value);
            }
        }

        public void AddPostVar(string key, object value)
        {
            if ( this.PostParms.ContainsKey(key) )
            {
                this.PostParms[key] = value;
            }
            else
            {
                this.PostParms.Add(key, value);
            }
        }

        public void Reset()
        {
            this.urlVars = new Dictionary<string, object>();
            this.callParms = new List<object>();
            this.PostParms = new Dictionary<string, object>();
            this.CallFunction = null;
        }

        public ResultSet DoCall()
        {
            if ( this.CallFunction.StartsWith("/") )
            {
                this.CallFunction = CallFunction.Substring(1);
            }

            if ( RdR.IsEmpty(this.FullURL) )
            {
                if ( this.CallFunction.StartsWith(this.DataURL) )
                {
                    this.FullURL = this.CallFunction;
                }
                else
                {
                    this.FullURL = this.DataURL + this.CallFunction;
                }
            }

            if ( this.callParms.Count > 0 )
            {
                if ( this.FullURL.EndsWith("/") )
                {
                    this.FullURL.TrimEnd('/');
                }

                foreach ( object dude in this.callParms )
                {
                    this.FullURL += "/" + dude.ToString();
                }
            }

            if ( this.urlVars.Count > 0 )
            {
                var lsAddMe = new List<string>();

                foreach ( string key in this.urlVars.Keys )
                {
                    lsAddMe.Add(string.Format("{0}={1}", key, this.urlVars[key]));
                }

                this.FullURL += "?" + string.Join("&", lsAddMe);
            }

            if ( this.StatusChanged != null )
            {
                this.StatusChanged(new StatusChangedArgs("Calling: " + this.FullURL));
            }

            var retry = false;

            while ( true )
            {
                var log = string.Format("{0}:{1}", this.authUser, this.authPass);
                var auth = Encoding.UTF8.GetBytes(log.ToCharArray());
                var wrCall = (HttpWebRequest)WebRequest.Create(this.FullURL);

                ServicePointManager.Expect100Continue = false;

                wrCall.Proxy = null;
                wrCall.Method = "GET";
                wrCall.Timeout = this.Timeout;
                wrCall.KeepAlive = false;
                wrCall.ContentType = "application/json";
                wrCall.ReadWriteTimeout = this.Timeout;
                wrCall.AllowWriteStreamBuffering = true;

                wrCall.Headers.Add("Authorization", "BASIC " + Convert.ToBase64String(auth));

                try
                {
                    if ( this.PostParms.Count > 0 )
                    {
                        var swOuttie = new StreamWriter(wrCall.GetRequestStream());

                        swOuttie.Write(JsonConvert.SerializeObject(this.PostParms));
                        swOuttie.Close();
                        swOuttie.Dispose();
                    }

                    var wrBack = wrCall.GetResponse();

                    if ( wrBack == null )
                    {
                        if ( this.StatusChanged != null )
                        {
                            this.StatusChanged(new StatusChangedArgs("Communication failed", StatusType.Error));
                        }

                        GC.Collect();

                        return this.CallResult;
                    }

                    var srBack = new StreamReader(wrBack.GetResponseStream());
                    var commData = srBack.ReadToEnd().Trim();

                    srBack.Close();
                    wrBack.Close();

                    srBack.Dispose();
                    wrBack.Dispose();

                    this.CallResult = new ResultSet(commData);
                    break;
                }
                catch ( Exception gotsError )
                {
                    if ( this.StatusChanged != null )
                    {
                        this.StatusChanged(new StatusChangedArgs(gotsError.Message, StatusType.Error));
                    }

                    if ( retry )
                    {
                        this.CallResult = ResultSet.MakeError("Failure: " + gotsError.Message);
                        break;
                    }
                    else
                    {
                        retry = true;
                    }
                }
            }

            GC.Collect();

            return this.CallResult;
        }
    }

    public class StatusChangedArgs : EventArgs
    {
        public string Message;
        public StatusType Type;

        public StatusChangedArgs(string passText = null, StatusType type = StatusType.Info)
        {
            this.Type = type;
            this.Message = passText;
        }
    }
}