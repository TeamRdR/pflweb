﻿using System;
using System.Collections;
using System.Data;

namespace PFL.Utils
{
    public class RdR
    {
        public static bool IsEqual(object what1, object what2)
        {
            if ( what1 == null && what2 == null )
            {
                return true;
            }

            if ( what1 == null || what2 == null )
            {
                return false;
            }

            var test1 = what1.ToString().ToLower();
            var test2 = what2.ToString().ToLower();

            return test1 == test2;
        }

        /// <summary>
        /// Fancy Method to check if a passed object is empty via object type or other.
        /// </summary>
        /// <param name="what">Any object string, number, DataTable, Array, or other</param>
        /// <returns>true/false if object is Empty</returns>
        public static bool IsEmpty(object what)
        {
            if ( what == null || what == DBNull.Value )
            {
                return true;
            }

            if ( what is Guid )
            {
                return (Guid)what == Guid.Empty;
            }

            if ( what is Array )
            {
                return ((Array)what).Length == 0;
            }

            if ( what is DataTable )
            {
                return ((DataTable)what).Rows.Count == 0;
            }

            if ( what is ICollection )
            {
                return ((ICollection)what).Count == 0;
            }

            if ( String.IsNullOrEmpty(what.ToString()) )
            {
                return true;
            }

            if ( decimal.TryParse(what.ToString(), out decimal xDude) )
            {
                return (xDude == 0);
            }

            if ( bool.TryParse(what.ToString(), out bool logi) && !logi )
            {
                return true;
            }

            return false;
        }

#region -----  GetString(+1) -----
        /// <summary>
        /// Esentially .ToString() but handles blank
        /// </summary>
        /// <param name="what">Any object string or otherwise</param>
        /// <returns>string value of object or "" {blank} if object is empty</returns>
        public static string GetString(object what)
        {
            return RdR.GetString(what, string.Empty);
        }

        /// <summary>
        /// Esentially .ToString(), but it allows for a specificed default using iDefault if IsEmpty()
        /// </summary>
        /// <param name="what">Any object string, number, or otherwise</param>
        /// <param name="aDefault">Default value to return if IsEmpty</param>
        /// <returns>string value of object or default if passed and object is empty</returns>
        public static string GetString(object what, string aDefault)
        {
            if ( RdR.IsEmpty(what) )
            {
                return aDefault;
            }

            return what.ToString();
        }
#endregion

    }
}