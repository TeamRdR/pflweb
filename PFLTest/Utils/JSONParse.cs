﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PFL.Models;

namespace PFL.Utils
{
    public class JSONParse
    {
        public static ArrayList GetArray(object jsonData)
        {
            var outtie = new ArrayList();

            if ( RdR.IsEmpty(jsonData) )
            {
                return outtie;
            }

            try
            {
                foreach ( var jObj in JArray.Parse(jsonData.ToString()) )
                {
                    outtie.Add(jObj);
                }
            }
            catch
            {
            }

            return outtie;
        }

        public static DataRow GetFirstRow(ResultSet callResults)
        {
            var dtTemp = JSONParse.GetDataTable(callResults.Data);

            if ( RdR.IsEmpty(dtTemp) )
            {
                return null;
            }

            return dtTemp.Rows[0];
        }

        public static DataTable GetDataTable(object jsonData)
        {
            return JSONParse.GetDataTable(jsonData, null);
        }

        public static DataTable GetDataTable(object jsonData, Dictionary<string, System.Type> dyColumnTypes)
        {
            if ( RdR.IsEmpty(jsonData) || RdR.IsEqual(jsonData, "{}") || RdR.IsEqual(jsonData, "[]") )
            {
                return new DataTable();
            }

            JArray jaDaddy = null;

            var dtRet = new DataTable();
            var strangIt = jsonData.ToString();

            // TODO:  Need a better method: Test if the JSON is an array, rather than just catching a parse exception
            try
            {
                jaDaddy = JArray.Parse(strangIt);
            }
            catch
            {
            }

            if ( jaDaddy == null )
            {
                var dyTemp = JsonConvert.DeserializeObject<Dictionary<string, object>>(strangIt);

                foreach ( var colName in dyTemp.Keys )
                {
                    dtRet.Columns.Add(colName);
                }

                var drTemp = dtRet.NewRow();

                foreach ( var colName in dyTemp.Keys )
                {
                    drTemp[colName] = dyTemp[colName];
                }

                dtRet.Rows.Add(drTemp);

                return dtRet;
            }

            JToken jtBaby;
            DataRow drNew = null;
            TypeCode tcCol;

            try
            {
                for ( int xRow = 0; xRow <= jaDaddy.Count - 1; xRow++ )
                {
                    jtBaby = jaDaddy[xRow];

                    if ( xRow == 0 )
                    {
                        foreach ( JProperty jtCol in jtBaby.Children() )
                        {
                            if ( dyColumnTypes != null && dyColumnTypes.ContainsKey(jtCol.Name) )
                            {
                                dtRet.Columns.Add(jtCol.Name, dyColumnTypes[jtCol.Name]);
                            }
                            else
                            {
                                dtRet.Columns.Add(jtCol.Name);
                            }
                        }
                    }

                    drNew = dtRet.NewRow();

                    foreach ( JProperty jtCol in jtBaby.Children() )
                    {
                        tcCol = Type.GetTypeCode(dtRet.Columns[jtCol.Name].DataType);

                        if ( tcCol == TypeCode.DateTime && RdR.IsEmpty(jtCol.Value) )
                        {
                            drNew[jtCol.Name] = DBNull.Value;
                            continue;
                        }

                        drNew[jtCol.Name] = jtCol.Value.ToString();
                    }

                    dtRet.Rows.Add(drNew);
                }
            }
            catch (Exception gotsError )
            {
            }

            return dtRet;
        }

        public static Dictionary<string, object> GetDictionary(object data)
        {
            var dyOuttie = new Dictionary<string, object>();

            if ( RdR.IsEmpty(data) )
            {
                return dyOuttie;
            }

            try
            {
                foreach ( JProperty jtCol in JObject.Parse(data.ToString()).Children() )
                {
                    dyOuttie.Add(jtCol.Name, jtCol.Value);
                }
            }
            catch (Exception gotsError )
            {
            }

            return dyOuttie;
        }
    }
}