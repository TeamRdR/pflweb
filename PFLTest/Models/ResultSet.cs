﻿
using System;
using Newtonsoft.Json.Linq;
using PFL.Utils;

namespace PFL.Models
{
    public class ResultSet
    {
        public object Data
        {
            get;
            set;
        }

        public int Status
        {
            get;
            set;
        }

        public string Message
        {
            get;
            set;
        }

        public ResultSet()
        {
            this.Status = ResultStatus.GOOD;
            this.Message = string.Empty;
        }

        public ResultSet(string commData)
        {
            if ( RdR.IsEmpty(commData) )
            {
                this.Status = ResultStatus.ERROR;
                this.Message = "No data to parse";
                return;
            }

            try
            {
                var joBeg = JObject.Parse(commData);
                var joRes = joBeg["results"];

                if ( RdR.IsEmpty(joRes["errors"]) )
                {
                    this.Data = joRes["data"];
                    this.Message = RdR.GetString(joRes["Message"]);
                }
                else
                {
                    this.Status = ResultStatus.ERROR;
                    //  TODO:  What does the "errors" array look like?
                   //     this.Message = joRes["errors"]
                }
            }
            catch ( Exception gotsError )
            {
                this.Status = ResultStatus.ERROR;
                this.Message = string.Format("{1}{0}{2}", Environment.NewLine, gotsError.Message, commData);
            }
        }

        public static ResultSet MakeError(string message)
        {
            return new ResultSet
            {
                Status = ResultStatus.ERROR,
                Message = message
            };
        }

        public static ResultSet MakeError(Exception gotsError)
        {
            return new ResultSet
            {
                Status = ResultStatus.ERROR,
                Message = gotsError.Message
            };
        }

        public static ResultSet MakeGood(object data = null)
        {
            var rsOutter = new ResultSet();

            rsOutter.Data = data;
            rsOutter.Status = ResultStatus.GOOD;

            return rsOutter;
        }
    }

    public class ResultStatus
    {
        public const int GOOD = 0;
        public const int ERROR = 1;
        public const int NODATA = 2;
        public const int INVALID = 3;
    }
}