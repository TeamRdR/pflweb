﻿using System.Web.Http;
using PFL.Utils;

namespace PFL.Controllers
{
    public class ProductController : ApiController
    {
        private const int APIKEY = 136085;

        [HttpGet]
        public IHttpActionResult List()
        {
            var dude = new PFLComm("products");

            dude.AddURLVar("apikey", APIKEY);

            var retRes = dude.DoCall();

            // TODO:  Sort the data

            return Ok(retRes.Data);
        }

        [HttpGet]
        public IHttpActionResult Detail(int prodID)
        {
            var dude = new PFLComm("products");

            dude.AddURLVar("apikey", APIKEY);
            dude.AddURLParameter(prodID);

            var retRes = dude.DoCall();

            return Ok(retRes.Data);
        }

        public void Order(object data)
        {
            System.Console.WriteLine(data);
            //var dude = new PFLComm("orders");

            //dude.AddURLVar("apikey", APIKEY);
        }
    }
}
