﻿using System.Collections.Generic;
using System.Web;
using System.Web.Optimization;

namespace PFL
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            // TODO:  Turn back on when in production
            BundleTable.EnableOptimizations = false;

            bundles.UseCdn = true;
            bundles.Add(new ScriptBundle("~/bundles/base").Include("~/Scripts/jquery-{version}.js", "~/Scripts/angular.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include("~/Scripts/modernizr-*"));
            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include("~/Scripts/bootstrap.js", "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/CustomJS").IncludeDirectory("~/Scripts/Custom", "*.js", true));

            bundles.Add(new StyleBundle("~/bundles/css").IncludeDirectory("~/Content", "*.css", true));
            bundles.Add(new StyleBundle("~/bundles/cssExt", "https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css"));
        }
    }
}