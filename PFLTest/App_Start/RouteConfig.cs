﻿using System.Web.Mvc;
using System.Web.Routing;

namespace PFL
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{prodID}",
                defaults: new { controller = "Home", action = "Index", prodID = UrlParameter.Optional }
            );
        }
    }
}
