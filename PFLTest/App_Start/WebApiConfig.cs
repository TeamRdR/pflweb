﻿using System.Net.Http.Headers;
using System.Web.Http;

namespace PFL
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "product/{action}/{prodid}",
                defaults: new { controller = "Product", prodID = RouteParameter.Optional }
            );
        }
    }
}
